import matplotlib.pyplot as plt


def square(list):
    product = []
    for a, b in zip(list, list):
        product.append(a*b)
    return(product)


def cube(list):
    product = []
    for a, b, c in zip(list, list, list):
        product.append(a*b*c)
    return product


x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
y = square(x)
plt.figure('1')
plt.plot(x, y)
y = cube(x)
plt.figure('2')
plt.plot(x, y)
plt.show()
