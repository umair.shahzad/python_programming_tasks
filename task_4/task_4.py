import argparse
import random
import json


def pi_f_carlo_monte(iterator):
    circle_points = 0
    square_points = 0
    for iterate in range(iterator):
        x = random.random()
        y = random.random()
        if((x**2+y**2) <= 1):
            circle_points += 1
        square_points += 1
    pi = 4*(circle_points/square_points)
    print(pi)


parser = argparse.ArgumentParser(
    description="Calculate pi from Monte Carlo's simulation")
parser.add_argument('-i', '--iterator', type=int, metavar='',
                    help="Number of iteration from user", default=5000)
parser.add_argument('-j', action="store_true",
                    help="Number of iteration from json file")
args = parser.parse_args()

if __name__ == "__main__":

    if(args.j):
        with open('iterator.json', 'r') as info:
            iterator = json.load(info)
            pi_f_carlo_monte(int(iterator['iterator']))
    else:
        pi_f_carlo_monte(args.iterator)
