
import math
import argparse

parser = argparse.ArgumentParser(description="calculate distance from origin")
parser.add_argument('-UP', type=int, metavar='',
                    help='Steps move in UP direction', default=0)
parser.add_argument('-DOWN', type=int, metavar='',
                    help='Steps move in DOWN direction', default=0)
parser.add_argument('-LEFT', type=int, metavar='',
                    help='Steps move in LEFT direction', default=0)
parser.add_argument('-RIGHT', type=int, metavar='',
                    help='Steps move in RIGHT direction', default=0)
args = parser.parse_args()


def pos_calc(UP, DOWN, LEFT, RIGHT):
    vert = UP - DOWN  # whether UP or DOWN +=UP -=DOWN
    hrzt = RIGHT - LEFT  # whether UP or DOWN +=UP -=DOWN
    dist_f_org(vert, hrzt)


def dist_f_org(vert, hrzt):
    dist = math.sqrt((vert**2) + (hrzt**2))
    print(round(dist))


if __name__ == "__main__":
    pos_calc(args.UP, args.DOWN, args.LEFT, args.RIGHT)
