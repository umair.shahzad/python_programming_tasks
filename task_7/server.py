#!/usr/bin/env python

import asyncio
import websockets
import json
import time
import logging
import argparse

logger = logging.getLogger(__name__)

formatter = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
file_handler = logging.FileHandler('server_log.log')
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)


def set_argument():
    parser = argparse.ArgumentParser(
        description="Websocket server")
    parser.add_argument('-c', '--console_log', action='store_false', default=True,
                        help="enable/disable console logging by default enabled")
    parser.add_argument('-f', '--file_log', action='store_true', default=False,
                        help="enable/disable console logging by default disabled")
    return parser.parse_args()


async def response(websocket):
    with open("msg_latency.txt", "w") as latency_file:
        logger.debug("New client connected to server!")
        async for msg in websocket:
            # msg = await websocket.recv()
            msg = json.loads(msg)
            logger.info(f"Message Received ")
            logger.debug(f"Message Received: {msg['method']}\n")
            msg["rx_time"] = time.time_ns()
            msg["msg_latency"] = msg["rx_time"]-msg["tx_time"]

            await websocket.send(json.dumps(msg))
            logger.info(f"Message Sent \n")
            logger.debug(f"Message Sent: {msg['method']}\n")
            latency_file.write(str(msg["msg_latency"])+'\n')
    logger.debug("client disconnected!")


async def main():

    async with websockets.serve(response, "localhost", 8765):
        await asyncio.Future()  # run forever

if __name__ == "__main__":

    args = set_argument()
    if(args.console_log == True):
        logger.addHandler(stream_handler)
        logger.setLevel(logging.INFO)

    if(args.file_log == True):
        logger.addHandler(file_handler)
        logger.setLevel(logging.DEBUG)
    asyncio.run(main())
