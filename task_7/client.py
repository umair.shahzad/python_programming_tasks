#!/usr/bin/env python

import asyncio
from os import path
import websockets
import json
import time
import logging
import argparse

logger = logging.getLogger(__name__)

formatter = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
file_handler = logging.FileHandler('client_log.log')
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)


def set_argument():
    parser = argparse.ArgumentParser(
        description="Websocket client")
    parser.add_argument('-PATH', type=str, metavar='',
                        help='Path to client_messages json file')
    parser.add_argument('-c', '--console_log', action='store_false', default=True,
                        help="enable/disable console logging by default enabled")
    parser.add_argument('-f', '--file_log', action='store_true', default=False,
                        help="enable/disable console logging by default disabled")
    return parser.parse_args()


async def handler():
    uri = "ws://localhost:8765"
    path = args.PATH
    f = open(path, 'r')
    info = json.load(f)
    async with websockets.connect(uri) as websocket:
        logger.debug("Connection established!")

        for message in info:
            message["tx_time"] = time.time_ns()
            await websocket.send(json.dumps(message))

            logger.info(f"Message Sent \n")
            logger.debug(f"Message Sent: {message['method']}\n")
            message_s = json.loads(await websocket.recv())
            logger.info(f"Message Received ")
            logger.debug(f"Message Received: {message_s['method']}\n")
    logger.debug("Connection closed!")


if __name__ == "__main__":
    args = set_argument()
    if(args.console_log == True):
        logger.addHandler(stream_handler)
        logger.setLevel(logging.INFO)

    if(args.file_log == True):
        logger.addHandler(file_handler)
        logger.setLevel(logging.DEBUG)
    asyncio.run(handler())
