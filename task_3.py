import psutil
import subprocess
import os


path = "/home/%s/Details/" % (os.getlogin())

if(os.path.exists == False):
    os.makedirs(path)

path += "summary.txt"

Id = (subprocess.check_output("lscpu ", shell=True).decode().split('\n'))
ram = (psutil.virtual_memory().total)/10**6
index_list = [0, 2, 4, 8, 13, 15, 16, 17, 19, 20, 21, 22, 23]
with open(path, 'w')as file:
    for index in index_list:
        file.write(Id[index]+"\n")
    file.write("RAM:\t\t\t\t "+str(ram)+" MB")
