import numpy as np
import datetime
data = np.loadtxt('msg_latency.txt')/1000

print('--------------------------------')
print(f"{datetime.datetime.now()}")
print("Stats for Websocket Latency")
print('--------------------------------')
print(f"Mean: {str(round(np.mean(data),3))}us")
print(f"Median: {str(round(np.median(data),3))}us")
print(f"Min: {str(round(np.min(data),3))}us")
print(f"Max: {str(round(np.max(data),3))}us")
print(f'Standard Deviation: {str(round(np.std(data),3))}us')
print(f'90th percentile: {str(round(np.percentile(data, 90),3))}')
print(f'99th percentile: {str(round(np.percentile(data, 99),3))}')
print(f'99.9th percentile: {str(round(np.percentile(data, 99.9),3))}')
print(f'99.99th percentile: {str(round(np.percentile(data, 99.99),3))}')
print(f'Total values: {len(data)}')
