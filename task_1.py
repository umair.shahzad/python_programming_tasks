
# Result generator from n1 to n2
def NumGen(n1, n2):
    result = []
    # iterate from n1 to n2
    for num in range(n1, n2+1):

        if num % 5 != 0 and num % 7 == 0:
            result.append(num)

    return result


# Driver code
if __name__ == "__main__":

    # Iterating over generator function
    result = NumGen(2000, 3200)
    for num in result:
        if(num == result[-1]):
            print(num)
            break
        print(num, end=',')
