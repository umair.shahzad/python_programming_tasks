import logging
import time
from pprint import pprint
logging.basicConfig(filename='task_6.log', filemode='w', level=logging.DEBUG)


def find_divisibles(in_range, divisor):
    logging.debug(
        "find_divisibles called with range %s and divisor %s" % (in_range, divisor))
    start_time = time.time()
    val_list = []
    for num in range(in_range):
        if(num % divisor == 0):
            val_list.append(num)
    end_time = time.time()
    logging.debug("find_divisibles ended with range %s and divisor %s,It took %s seconds" % (
        in_range, divisor, (end_time-start_time)))

    return val_list


if __name__ == "__main__":
    list1 = find_divisibles(50800000, 34113)
    list2 = find_divisibles(100052, 3210)
    list3 = find_divisibles(500, 3)
    pprint(list2)
    pprint(list3)
