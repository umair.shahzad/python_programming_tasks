import logging
from pprint import pprint
import time
import asyncio
logging.basicConfig(filename='task_6.log', filemode='w', level=logging.DEBUG)


async def find_divisibles(in_range, divisor):
    logging.debug(
        "find_divisibles called with range %s and divisor %s" % (in_range, divisor))
    start_time = time.time()
    val_list = []
    for num in range(in_range):
        if(num % divisor == 0):
            val_list.append(num)
            await asyncio.sleep(0)
    end_time = time.time()
    logging.debug("find_divisibles ended with range %s and divisor %s,It took %s seconds" % (
        in_range, divisor, (end_time-start_time)))

    return val_list


async def main():
    task1 = asyncio.create_task(find_divisibles(50800000, 34113))
    task2 = asyncio.create_task(find_divisibles(100052, 3210))
    task3 = asyncio.create_task(find_divisibles(500, 3))

    await asyncio.wait([task1, task2, task3])

    pprint(task2.result())
    pprint(task2.result())


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
